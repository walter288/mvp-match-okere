import { MVPApiResponse } from '../../models/ApiResponse';
import { Gateways } from '../../models/Gateways';
import { Projects } from '../../models/Projects';
import { Reports, ReportsPayload } from '../../models/Reports';
import { MVPUser } from '../../models/Users';
import axios from '../MvpAxiosConfiguration';

/**
 * Get all users
 */
export const getUsers = async () => {
    const { data } = await axios.get<MVPApiResponse<MVPUser>>(`users`);
    return data
}

/**
 * Get all projects
 */
 export const getAllProjects = async () => {
    const { data } = await axios.get<MVPApiResponse<Projects[]>>(`projects`);
    return data
}

/**
 * Get all gateways
 */
 export const getAllGateways = async () => {
    const { data } = await axios.get<MVPApiResponse<Gateways[]>>(`gateways`);
    return data
}


/** 
 * Generate reports taken by filter
*/
export const generateReports = async (payload: ReportsPayload) => {
    const { data } = await axios.post<MVPApiResponse<Reports>>(`report`, payload);
    return data
}

import React, { FC } from 'react';
import { BrowserRouter as Router, Redirect, Route, Switch } from "react-router-dom";
import PHomePage from '../pages/Home';

interface RouteProps {}
export const MVPRoutes: FC = (props: RouteProps) => {
    return (
        <Router>
            <Switch>
                <Route path="/">
                    <PHomePage />
                </Route>
            </Switch>
        </Router>
    );
};
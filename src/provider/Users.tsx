import React, { ReactNode, ReactElement } from 'react';
import { MVPUser } from '../models/Users';
import { getUsers } from '../services/MvpApis';

type UserContext = {
    users: MVPUser[];
    setUser: React.Dispatch<React.SetStateAction<MVPUser[]>>;
};

const UserContext = React.createContext<UserContext>({
    users: [] as MVPUser[],
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    setUser: () => { }
});

const UserProvider = ({
    children
}: {
    children: ReactNode;
}): ReactElement => {
    const [users, setUser] = React.useState<MVPUser[]>([] as MVPUser[]);
    React.useEffect(() => {
        const initializeUser = async (): Promise<void> => {
            getUsers().then(user => {
                if (user?.code === '200') setUser(user.data);                
            }).catch(error => console.error(error));
        };
        initializeUser();
    }, []);


    return (
        <UserContext.Provider
            value={{
                users,
                setUser,
            }}
        >
            {children}
        </UserContext.Provider>
    );
};

export default UserProvider;

export function UseUser(): UserContext {
    const context = React.useContext(UserContext);
    if (context === undefined) {
        throw new Error('useUsers must be used within an userProvider');
    }
    return context;
}

export function retrievedUser(): MVPUser[] {
    const context = UseUser();
    return context.users;
}
import axios from 'axios';
import React, { ReactNode, ReactElement, useState, useEffect, useRef } from 'react';
import { MVPApiResponse } from '../models/ApiResponse';
import { Gateways } from '../models/Gateways';
import { Projects } from '../models/Projects';
import { MVPUser } from '../models/Users';
import { getAllGateways, getAllProjects, getUsers } from '../services/MvpApis';
import { retrievedUser } from './Users';

type ProjectAndGatewayContext = {
    mvpProjects: Projects[];
    mvpGateways: Gateways[]
    setMvpProjects: React.Dispatch<React.SetStateAction<Projects[]>>;
    setMvpGateways: React.Dispatch<React.SetStateAction<Gateways[]>>;
};

const ProjectAndGatewayContext = React.createContext<ProjectAndGatewayContext>({
    mvpProjects: [] as Projects[],
    mvpGateways: [] as Gateways[],
    setMvpProjects: () => { },
    setMvpGateways: () => { },

});

const ProjectAndGatewayProvider = ({
    children
}: {
    children: ReactNode;
}): ReactElement => {
    const user = retrievedUser()[0] || {} as MVPUser;
    const [mvpProjects, setMvpProjects] = useState<Projects[]>([] as Projects[]);
    const [mvpGateways, setMvpGateways] = useState<Gateways[]>([] as Gateways[]);
    const hasUser = useRef<boolean>(false);
    useEffect(() => {
        const initializeProjectsAndGateways = () => {
            axios.all<{}>([getAllProjects(), getAllGateways()]).then(axios.spread((...response) => {
                const projects = response[0] as MVPApiResponse<Projects>;
                const gateways = response[1] as MVPApiResponse<Gateways>;
                if (projects?.code === '200' && gateways?.code === '200') {
                    setMvpProjects(projects.data.filter(o => o.userIds.includes(user?.userId)));
                    setMvpGateways(gateways.data.filter(o => o.userIds.includes(user?.userId)));
                }
            })).catch(errors => console.log(errors));
        };
        if (user?.userId && !hasUser.current) {
            hasUser.current = true;
            initializeProjectsAndGateways();
        }
    }, [user?.userId])


    return (
        <ProjectAndGatewayContext.Provider
            value={{
                mvpProjects,
                mvpGateways,
                setMvpProjects,
                setMvpGateways
            }}
        >
            {children}
        </ProjectAndGatewayContext.Provider>
    );
};

export default ProjectAndGatewayProvider;

export function UseProjectAndGateway(): ProjectAndGatewayContext {
    const context = React.useContext(ProjectAndGatewayContext);
    if (context === undefined) {
        throw new Error('UseProjectAndGateway must be used within an ProjectAndGatewayProvider');
    }
    return context;
}
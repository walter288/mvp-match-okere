import React, { ReactNode, ReactElement, useState, useEffect } from 'react';
import { Reports, ReportsPayload } from '../models/Reports';
import { generateReports } from '../services/MvpApis';
import { useHistory, useRouteMatch, useParams } from 'react-router-dom';
import { DropdownType } from '../components/shared/ContentHeader';
import { UseProjectAndGateway } from './ProjectsAndGateway';
import { Action, History } from 'history';
import { Location } from 'react-router';

type ReportsContext = {
    mvpReports: Reports[];
    onUpdateReport: (reportsPayload: ReportsPayload) => void;
    setSelectedProject: React.Dispatch<React.SetStateAction<string>>;
    setSelectedGateway: React.Dispatch<React.SetStateAction<string>>;
    selectedProject: string;
    selectedGateway: string;
};

const ReportsContext = React.createContext<ReportsContext>({
    mvpReports: [] as Reports[],
    onUpdateReport: () => { },
    setSelectedProject: () => { },
    setSelectedGateway: () => { },
    selectedProject: {} as string,
    selectedGateway: {} as string
});

const ReportsProvider = ({
    children
}: {
    children: ReactNode;
}): ReactElement => {
    const history: History<unknown> = useHistory();
    let { url, path } = useRouteMatch();
    const { mvpProjects, mvpGateways } = UseProjectAndGateway();
    let { gatewayId } = useParams();
    const [mvpReports, setMvpReports] = useState<Reports[]>([] as Reports[]);
    const [selectedProject, setSelectedProject] = useState<string>('All Projects')
    const [selectedGateway, setSelectedGateway] = useState<string>('All Gateways')

    useEffect(() => {
        initializeReports(new ReportsPayload());
        const unlisten = history.listen(listenedToRouteChange);
        return unlisten
    }, [])

    const getProjectName = (projectId: string): string => {
        return mvpProjects.find(o => o.projectId === projectId)?.name || 'All Projects'
    }

    const getGatewayName = (gatewayId: string): string => {
        console.log(gatewayId)
        return mvpGateways.find(o => o.gatewayId === gatewayId)?.name || 'All Gateways'
    }

    const listenedToRouteChange = (location: any, action: Action) => {
        if (action === 'POP') {
            console.log('yes')
            initializeReports(new ReportsPayload());
        }
    }

    const initializeReports = (payload: ReportsPayload) => {
        console.log(payload);
        generateReports(payload).then(report => {
            if (report?.code === '200') {
                if (report?.data.length === 0) {
                    history.push('/no-data');
                } else {
                    setSelectedProject(getProjectName(payload?.projectId));
                    setSelectedGateway(getGatewayName(payload?.gatewayId));
                    const gatewayParam = window.location.pathname.split('/')[1];
                    setMvpReports(report?.data);
                    if (payload.gatewayId !== '' && payload.projectId !== '') {
                        history.push(`/${payload?.gatewayId}`);
                    } else if (gatewayParam && payload.gatewayId === '' || payload.projectId === '') {
                        history.replace('/');
                    } else if (gatewayParam && !payload.gatewayId || !payload.projectId) {
                        history.replace('/');
                    }
                }
            }
        }).catch(errors => console.log(errors));
    }

    const onUpdateReport = (payload: ReportsPayload): void => {
        initializeReports(payload)
    }


    return (
        <ReportsContext.Provider
            value={{
                mvpReports,
                onUpdateReport,
                setSelectedProject,
                setSelectedGateway,
                selectedProject,
                selectedGateway
            }}
        >
            {children}
        </ReportsContext.Provider>
    );
};

export default ReportsProvider;

export function UseReports(): ReportsContext {
    const context = React.useContext(ReportsContext);
    if (context === undefined) {
        throw new Error('UseReports must be used within an ReportsProvider');
    }
    return context;
}
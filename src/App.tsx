import React from 'react';
import './App.scss';
import { MVPRoutes } from './routes';

function App() {
  return (
    <MVPRoutes />
  );
}

export default App;

import React, { FC, useEffect, useState } from 'react';
import ReactApexChart from 'react-apexcharts';
import { Gateways } from '../../../models/Gateways';
import { Projects } from '../../../models/Projects';


interface MVPChartProps {
    chartSeries?: Array<number>,
    mvpProjects: Projects[] | Gateways[],
    dataSet?: number[]
};
export const MVPChart: FC<MVPChartProps> = (props: MVPChartProps) => {
    const { chartSeries, mvpProjects, dataSet } = props;

    const [options, setOptions] = useState<ApexCharts.ApexOptions>({
        chart: {
            type: 'donut',
            parentHeightOffset: 50
        },
        colors: ['#A259FF', '#FFC107', '#F24E1E', '#6497B1'],
        responsive: [{
            breakpoint: 480,
            options: {
                chart: {
                    width: 100,
                },
                legend: {
                    position: 'bottom',
                }
            }
        }],
        legend: {
            show: true,
            position: 'top',
            height: 50,
            customLegendItems: mvpProjects.map((project) => project.name),
            itemMargin: {
                horizontal: 35,
                vertical: 0
            },
            offsetX: 0,
            offsetY: 0,
            markers: {
                width: 12,
                height: 12,
                radius: 4,
            },
        },
        grid: {
            show: true,
            padding: {
                top: 80,
                right: 0,
                bottom: 80,
                left: 0
            },  
        },
        tooltip: {
            enabled: false
        }
    })

    return (
        <div id="chart" style={{ width: '100%' }}>
            <ReactApexChart options={options} series={dataSet || []} type="donut" />
        </div>
    );
};
import React, { FC } from 'react';

interface ChartLegendProps {
    legendData: Array<{ title: string; color: string; }>
};
export const ChartLegend: FC<ChartLegendProps> = (props: ChartLegendProps) => {
    const { legendData } = props
    return (
        <div className="mvp-content-panel-report flex items-center justify-between w-full">
            {(legendData || []).map((o, i) => (
                <div key={i} className="flex items-center gap-2">
                    <span className={`chart-color ${o.color}`}></span>
                    <span>{o.title}</span>
                </div>
            ))}
        </div>
    );
};
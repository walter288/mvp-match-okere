import React, { FC } from 'react';
import css from './Breadcrumb.module.scss'

interface BreadcrumbProps {
    titles: Array<string>
};
export const Breadcrumb: FC<BreadcrumbProps> = (props: BreadcrumbProps) => {
    const { titles } = props;
    return (
        <div className={css.Breadcrumb}>
            {titles.map((o, i) => (
                <span key={i}> {o} <b>{ titles[titles.length -1] !== o && '|' }</b></span>
            ))}
        </div>
    );
};
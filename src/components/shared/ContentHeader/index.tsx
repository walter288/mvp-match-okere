import React, { useState, useEffect } from 'react';
import DatePicker from 'react-datepicker';
import { Gateways } from '../../../models/Gateways';
import { Projects } from '../../../models/Projects';
import { UseProjectAndGateway } from '../../../provider/ProjectsAndGateway';
import { MVPButton } from '../UI/Button';
import { MVPDatepicker } from '../UI/Datepicker';
import MVPDropdown from '../UI/Dropdown';
import css from './ContentHeader.module.scss';
import { format } from 'date-fns'
import { ReportsPayload } from '../../../models/Reports';
import { IsObject, IsObjectEmpty } from '../../../utils/helper';
import { UseReports } from '../../../provider/Reports';

interface ContentHeaderProps {

};
export type DropdownType = {id: string, name: string };

const ContentHeader = (props: ContentHeaderProps) => {
    const { mvpProjects, mvpGateways } = UseProjectAndGateway();
    const { onUpdateReport } = UseReports();
    const [startDate, setStartDate] = useState<Date | null | undefined>();
    const [endDate, setEndDate] = useState<Date | null | undefined>()
    const [selectedProject, setSelectedProject] = useState<DropdownType>({} as DropdownType)
    const [selectedGateway, setSelectedGateway] = useState<DropdownType>({} as DropdownType)


    const defineIdsInOther = <T extends {}>(items: Array<T>, paramKey: keyof T, paramName: keyof T) => {
        return items.map(({ ...item }) => ({
            id: item[paramKey],
            name: item[paramName],
        }))
    }

    const handleChangeStartDate = (date: unknown) => {
        const takenDate = JSON.stringify(date);
        if (typeof takenDate === 'string') setStartDate(new Date(JSON.parse(takenDate)));
    }

    const handleChangeEndDate = (date: unknown) => {
        const takenDate = JSON.stringify(date);
        if (typeof takenDate === 'string') setEndDate(new Date(JSON.parse(takenDate)));
    }

    const handleProjectsChange = (takenProject: DropdownType) => {
        console.log(takenProject)
        setSelectedProject(takenProject)
    }

    const handleGatewayChange = (takenGateway: DropdownType) => {
        console.log(takenGateway)
        setSelectedGateway(takenGateway)
    }

    const handleGenerateReport = (e: React.MouseEvent<HTMLButtonElement>) => {
        const from = startDate ? format(new Date(startDate as Date), 'yyyy-MM-dd') : '';
        const to = endDate ? format(new Date(endDate as Date), 'yyyy-MM-dd') : '';
        const projectId = IsObject(selectedProject) && !IsObjectEmpty(selectedProject) ? selectedProject?.id : ''
        const gatewayId = IsObject(selectedGateway) && !IsObjectEmpty(selectedGateway) ? selectedGateway?.id : ''
        const reportPayload = new ReportsPayload(from, to, projectId, gatewayId);
        onUpdateReport(reportPayload);
    }

    return (
        <div className="grid grid-cols-3 py-7 gap-16">
            <div className="flex flex-col">
                <h4 className={css.ContentHeaderTitle}>Reports</h4>
                <p className={css.ContentHeaderSubTitle}>Easily generate a report of your transactions</p>
            </div>
            <div className="col-span-2">
                <div className="flex align-center gap-4">
                    <MVPDropdown items={defineIdsInOther(mvpProjects, 'projectId', 'name')} title="All Projects" selected={selectedProject} onChange={handleProjectsChange} />
                    <MVPDropdown items={defineIdsInOther(mvpGateways, 'gatewayId', 'name')} title="All Gateways" selected={selectedGateway} onChange={handleGatewayChange} />
                    <MVPDatepicker startDate={startDate} handleChange={handleChangeStartDate} dateTypeStartDate={true} />
                    <MVPDatepicker startDate={endDate} handleChange={handleChangeEndDate} dateTypeStartDate={false} />
                    <MVPButton title="Generate report" onClickTrigger={handleGenerateReport} />
                </div>
            </div>
        </div>
    );
};

export default ContentHeader
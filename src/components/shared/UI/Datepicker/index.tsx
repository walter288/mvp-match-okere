import React, { forwardRef } from 'react'
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";
import css from './Datepicker.module.scss';
import CalenderIcon from '../../../../assets/images/calender.png'

type StartDateStateType = Date | [Date | null, Date | null] | null;
interface Props {
    startDate: Date | null | undefined;
    handleChange: (date: StartDateStateType) => void;
    dateTypeStartDate: boolean;
};

export const MVPDatepicker = (props: Props) => {
    const { startDate, handleChange, dateTypeStartDate } = props;
    return (
        <DatePicker
            selected={startDate}
            onChange={(date) => handleChange(date)}
            startDate={startDate}
            customInput={dateTypeStartDate ? <StartDateButtonInput /> : <EndDateButtonInput />}
        />
    );
};

const StartDateButtonInput = forwardRef<HTMLButtonElement, React.DetailedHTMLProps<
    React.ButtonHTMLAttributes<HTMLButtonElement>,
    HTMLButtonElement
>>((props, ref) => (
    <button
        {...props}
        ref={ref}
        type="button"
        className={`${css.ButtonColor} mt-1 pl-5 pr-5 transition-colors duration-700 transform text-white rounded-lg focus:border-4 border-indigo-300`}
    >
        <div className="flex justify-between items-center">
            <span>{props.value ? props.value : 'From date'}</span>
            <img src={CalenderIcon} alt="Calender Icon" />
        </div>
    </button>
))

const EndDateButtonInput = forwardRef<HTMLButtonElement, React.DetailedHTMLProps<
    React.ButtonHTMLAttributes<HTMLButtonElement>,
    HTMLButtonElement
>>((props, ref) => (
    <button
        {...props}
        ref={ref}
        type="button"
        className={`${css.ButtonColor} mt-1 pl-5 pr-5 transition-colors duration-700 transform text-white rounded-lg focus:border-4 border-indigo-300`}
    >
        <div className="flex justify-between items-center">
            <span>{props.value ? props.value : 'End date'}</span>
            <img src={CalenderIcon} alt="Calender Icon" />
        </div>
    </button>
))
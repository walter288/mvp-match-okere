import * as React from 'react';
import css from './Button.module.scss'

interface MVPButtonProps {
    title: string;
    onClickTrigger: (e: React.MouseEvent<HTMLButtonElement>) => void;
};
export const MVPButton = (props: MVPButtonProps) => {
    const { title, onClickTrigger } = props;
    return (
        <button onClick={onClickTrigger} className={`${css.ButtonColor} mt-1 p-1 pl-5 pr-5 transition-colors duration-700 transform hover:bg-blue-400 text-white rounded-lg focus:border-4 border-indigo-300`}>{title}</button>);
};
import { Fragment, useState, FC } from 'react'
import { Listbox, Transition } from '@headlessui/react'
import { CheckIcon, ChevronDownIcon } from '@heroicons/react/solid'
import css from './Dropdown.module.scss'
import { DropdownType } from '../../ContentHeader'

type itemDataType = { id: string | number | string[] | undefined; name: string | number | string[] | undefined; }

interface MVPDropdownProps<T extends itemDataType>{
  items: T[];
  title: string;
  onChange: (selected: DropdownType) => void;
  selected: T
}

const MVPDropdown: FC<MVPDropdownProps<itemDataType>> = (props: MVPDropdownProps<itemDataType>) => {
  const { items, selected, title, onChange } = props;

  const updatedItem = (): itemDataType[] => {
    let tempData = items.slice();
    tempData.unshift({id: '', name: title})
    return tempData
  }

  return (
    <div className="w-36">
      <Listbox value={selected} onChange={onChange}>
        <div className="relative mt-1">
          <Listbox.Button className={`${css.DropdownColor} relative w-full py-2 pl-3 pr-10 text-left bg-white rounded-lg cursor-default focus:outline-none focus-visible:ring-2 focus-visible:ring-opacity-75 focus-visible:ring-white focus-visible:ring-offset-orange-300 focus-visible:ring-offset-2 focus-visible:border-indigo-500 sm:text-sm`}>
            <span className="block truncate">{selected?.name || title }</span>
            <span className="absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
              <ChevronDownIcon
                className="w-5 h-5 text-white"
                aria-hidden="true"
              />
            </span>
          </Listbox.Button>
          <Transition
            as={Fragment}
            leave="transition ease-in duration-100"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <Listbox.Options className={`${css.DropdownColor} absolute w-full py-1 mt-1 overflow-auto rounded-md shadow-lg max-h-60 ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm`}>
              {updatedItem().map((o: any, oIdx) => (
                <Listbox.Option
                  key={oIdx}
                  className={({ active }) =>
                    `${active ? 'text-grey-900 bg-amber-100' : 'text-white-900'}
                          cursor-default select-none relative py-2 pl-10 pr-4`
                  }
                  value={o}
                >
                  {({ selected, active }) => (
                    <>
                      <span
                        className={`${
                          selected ? 'font-medium' : 'font-normal'
                        } block truncate`}
                      >
                        {o.name}
                      </span>
                      {selected ? (
                        <span
                          className={`${
                            active ? 'text-amber-600' : 'text-amber-600'
                          }
                                absolute inset-y-0 left-0 flex items-center pl-3`}
                        >
                          <CheckIcon className="w-5 h-5" aria-hidden="true" />
                        </span>
                      ) : null}
                    </>
                  )}
                </Listbox.Option>
              ))}
            </Listbox.Options>
          </Transition>
        </div>
      </Listbox>
    </div>
  )
}

export default MVPDropdown;
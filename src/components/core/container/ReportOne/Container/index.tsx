import React, { FC } from 'react';
import MVPLayout from '../../../layout';
import { Switch, useRouteMatch, Route } from 'react-router-dom';
import MVPHome from '../Home';
import { MVPGateway } from '../Gateway';
import { NoDataFound } from '../NoData';

interface ReportOneContainerProps {
};
const ReportOneContainer: FC<ReportOneContainerProps> = (props: ReportOneContainerProps) => {
    let { url } = useRouteMatch();

    return (
        <MVPLayout>
            <div className="flex flex-col gap-5">
                <Switch>
                    <Route path="/no-data">
                        <NoDataFound />
                    </Route>
                    <Route path={`/:gatewayId`}>
                        <MVPGateway />
                    </Route>
                    <Route path="/">
                        <MVPHome />
                    </Route>
                </Switch>
            </div>
        </MVPLayout>
    );
};

export default ReportOneContainer;
import React from 'react';
import { FC } from 'react';
import { UseProjectAndGateway } from '../../../../../provider/ProjectsAndGateway';
import { UseReports } from '../../../../../provider/Reports';
import { Breadcrumb } from '../../../../shared/Breadcrumb';

interface MVPGatewayProps {

};
export const MVPGateway: FC<MVPGatewayProps> = (props: MVPGatewayProps) => {
    const { mvpReports, selectedProject, selectedGateway } = UseReports();
    const { mvpGateways } = UseProjectAndGateway();

    const getGatewayName = (gatewayId: string): string => {
        return mvpGateways.find(o => o.gatewayId === gatewayId)?.name || ''
    }

    const calculateTotalGatewayAmount = (): number => {
        return mvpReports.reduce((acc, o) => acc + Number(o.amount), 0);
    }


    return (
        <>
            <div className="mvp-content-panel" style={{ maxHeight: '50vh', overflowY: 'auto' }}>
                <Breadcrumb titles={[selectedProject, selectedGateway]} />

                <div className="w-full pt-6">
                    <div className="w-full mx-auto rounded-2xl">
                        <table className="mvp-table">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <td>Gateway</td>
                                    <th>Transaction ID</th>
                                    <th>Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    mvpReports.map((o, i) => (
                                        <tr key={i}>
                                            <td>{o.created}</td>
                                            <td>{getGatewayName(o.gatewayId)}</td>
                                            <td>{o.paymentId}</td>
                                            <td>{o.amount}</td>
                                        </tr>
                                    ))
                                }
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
            <div className="mvp-content-bottom-panel py-1">
                <span>TOTAL: {calculateTotalGatewayAmount()}</span>
            </div>
        </>
    );
};
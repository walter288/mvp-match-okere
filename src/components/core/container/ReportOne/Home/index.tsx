import React, { FC, useEffect, useState } from 'react';
import { Disclosure } from '@headlessui/react'
import css from './Home.module.scss'
import { UseProjectAndGateway } from '../../../../../provider/ProjectsAndGateway';
import { UseReports } from '../../../../../provider/Reports';
import { Reports } from '../../../../../models/Reports'
import { Breadcrumb } from '../../../../shared/Breadcrumb';
import { groupBy, orderBy } from "lodash";
import { MVPChart } from '../../../../shared/Chart';

export interface MvpGroupedItem {
    [key: string]: Reports[]
}

interface MVPHomeProps {
};

const MVPHome: FC<MVPHomeProps> = (props: MVPHomeProps) => {
    const { mvpReports, selectedProject, selectedGateway } = UseReports();
    const { mvpProjects, mvpGateways } = UseProjectAndGateway();
    const [groupedData, setGroupedData] = useState<MvpGroupedItem>();

    useEffect(() => {
        if (isWithoutGraph()) {
            setGroupedData(groupBy(orderBy(mvpReports, ['projectId'], ['desc']), 'projectId'));
        }
        if (isWithProjects()) {
            setGroupedData(groupBy(orderBy(mvpReports, ['projectId'], ['desc']), 'projectId'));
        }
        if (isWithGateway()) {
            setGroupedData(groupBy(orderBy(mvpReports, ['gatewayId'], ['desc']), 'gatewayId'));
        }
    }, [mvpReports, selectedProject, selectedGateway])

    const calculateProjectAmount = (data: Reports[]): string => {
        return data?.map((report) => report.amount).reduce(
            (previousValue: number, currentValue: number) =>
                previousValue + currentValue,
            0
        )
            ?.toFixed(2);
    }

    const getProjectName = (projectId: string): string => {
        return mvpProjects.find(o => o.projectId === projectId)?.name || ''
    }

    const getGatewayName = (gatewayId: string): string => {
        console.log(gatewayId)
        return mvpGateways.find(o => o.gatewayId === gatewayId)?.name || ''
    }

    const isWithoutGraph = (): boolean => {
        return selectedProject === 'All Projects' && selectedGateway === 'All Gateways'
    }

    const isWithProjects = (): boolean => {
        return selectedProject === 'All Projects' && selectedGateway !== 'All Gateways';
    }

    const isWithGateway = (): boolean => {
        return selectedProject !== 'All Projects' && selectedGateway === 'All Gateways'
    }

    return (
        <div className={!isWithoutGraph() ? 'grid grid-cols-2 gap-2' : ''} style={{ minHeight: 520 }}>
            <div className={isWithoutGraph() ? 'mvp-content-panel' : 'mvp-content-panel-report col-span-1'}>
                <Breadcrumb titles={[selectedProject, selectedGateway]} />
                <div className={!isWithoutGraph() ? 'w-full pt-6' : 'w-full pt-4'}>
                    <div className="w-full mx-auto rounded-2xl" style={{ maxHeight: '50vh', overflowY: 'auto' }}>
                        {groupedData &&
                            Object.entries(groupedData).map((o, i) => (
                                <Disclosure key={i} as="div" defaultOpen={i === 0 && true} className="mt-2">
                                    {({ open }) => (
                                        <>
                                            <Disclosure.Button className={`${css.ListTitleColor} flex justify-between w-full px-4 py-3 text-sm font-medium text-left text-black-900 bg-white rounded-lg focus:outline-none focus-visible:ring focus-visible:ring-purple-500 focus-visible:ring-opacity-75`}>
                                                <span>{isWithGateway() ? getGatewayName(o[0]) : getProjectName(o[0])}</span>
                                                <span>TOTAL: {calculateProjectAmount(o[1])} USD</span>
                                            </Disclosure.Button>
                                            <Disclosure.Panel className="pl-4 pt-4 pb-2 text-sm text-gray-500">
                                                <table className="mvp-table">
                                                    <thead>
                                                        <tr>
                                                            <th>Date</th>
                                                            {isWithoutGraph() &&
                                                                <td>Gateway</td>
                                                            }
                                                            <th>Transaction ID</th>
                                                            <th>Amount</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        {
                                                            o[1].map((o, i) => (
                                                                <tr key={i}>
                                                                    <td>{o.created}</td>
                                                                    {isWithoutGraph() &&
                                                                        <td>{getGatewayName(o.gatewayId)}</td>
                                                                    }
                                                                    <td>{o.paymentId}</td>
                                                                    <td>{o.amount}</td>
                                                                </tr>
                                                            ))
                                                        }
                                                    </tbody>
                                                </table>

                                            </Disclosure.Panel>
                                        </>
                                    )}
                                </Disclosure>
                            ))}
                    </div>
                </div>
            </div>
            {!isWithoutGraph() &&
                <div className="bg-white py-1 col-span-1 ml-6 mr-20">
                    <div className="flex flex-col items-center w-full justify-between h-full">
                        <MVPChart
                            mvpProjects={isWithProjects() ? mvpProjects : mvpGateways}
                            dataSet={groupedData ? Object.values(groupedData).map((data) => data.length) : []}
                        />
                        <div className="mvp-content-bottom-panel-report py-1 w-full">
                            <span>{ isWithProjects() && 'GATEWAYS'}{isWithGateway() && 'PROJECTS'} TOTAL | {calculateProjectAmount(mvpReports)} USD</span>
                        </div>
                    </div>
                </div>
            }

            {
                isWithoutGraph() &&
                <div className="mvp-content-bottom-panel py-1 mt-5">
                    <span>TOTAL: {calculateProjectAmount(mvpReports)}</span>
                </div>

            }
        </div>

    );
};

export default MVPHome;
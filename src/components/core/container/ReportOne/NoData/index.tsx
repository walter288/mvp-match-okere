import React, { FC } from 'react';
import NoData from '../../../../../assets/images/no-data.svg';
import css from './NoData.module.scss'

interface NoDataFoundProps {

};
export const NoDataFound: FC<NoDataFoundProps> = (props: NoDataFoundProps) => {
    return (
        <div className="w-full h-full flex justify-center items-center">
            <div className="flex flex-col justify-center items-center" style={{ height: 500, width: 500 }}>
                <h2 className={css.HeaderTitleColor}>No reports</h2>
                <p className={css.ContentColor}>Currently you have no data for the reports to be generated.
                    Once you start generating traffic through the Balance application
                    the reports will be shown.</p>
                    <img src={NoData} alt="" />
            </div>
        </div>
    );
};
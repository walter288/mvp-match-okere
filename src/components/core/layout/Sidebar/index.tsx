import React, { FC } from 'react';
import css from './Sidebar.module.scss';
import AnalyticIcon from "./../../../../assets/images/AnalyticLogo.png";
import HomeIcon from "./../../../../assets/images/HomeLogo.png";
import CardIcon from "./../../../../assets/images/CardLogo.png";
import GraphIcon from "./../../../../assets/images/GraphLogo.png";
import LogoutIcon from "./../../../../assets/images/LogoutLogo.png";

interface SidebarProps {

};

const Sidebar: FC<SidebarProps> = (props: SidebarProps) => {
    return (
        <nav className={css.Navbar}>
            <img
                src={AnalyticIcon}
                alt="Analityc Logo" />
            <img src={HomeIcon} alt="Analityc Logo" />
            <img src={CardIcon} alt="Analityc Logo" />
            <img src={GraphIcon} alt="Analityc Logo" />
            <img src={LogoutIcon} alt="Analityc Logo" />
        </nav>
    );
};

export default Sidebar;
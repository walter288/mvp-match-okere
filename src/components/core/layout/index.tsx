import React, { FC, ReactElement } from 'react';
import ContentHeader from '../../shared/ContentHeader';
import MVPHeader from './Header';
import css from './Layout.module.scss';
import Sidebar from './Sidebar';

interface MVPLayoutProps {
    children: ReactElement;
};

const MVPLayout: FC<MVPLayoutProps> = (props: MVPLayoutProps) => {
    const { children } = props;
    return (
        <>
            <MVPHeader />
            <div>
                <Sidebar />
                <main className={css.MainContent}>
                    <div className="flex flex-col w-full">
                    <ContentHeader />
                    { children }
                    </div>
                </main>
                <footer className="fixed bottom-0 ml-24 py-3 z-10">
                    <div className="footer-breadcrumb">
                        <span>Terms & Conditions</span> | <span>Privacy policy</span>
                    </div>
                </footer>
            </div>
        </>
    );
};

export default MVPLayout;
import React, { FC } from 'react';
import css from './Header.module.scss';
import Logo from '../../../../assets/images/logo.svg';
import Menu from '../../../../assets/images/menu.svg';
import { retrievedUser } from '../../../../provider/Users';
import { MVPUser } from '../../../../models/Users';

interface HeaderProps {};

const MVPHeader: FC<HeaderProps> = (props: HeaderProps) => {
    const user = retrievedUser()[0] || {} as MVPUser;

    const takeIstLetters = (firstname: string, lastname: string): string => {
        return `${firstname?.split('')[0]} ${lastname?.split('')[0]}`;
    }

    return (
        <header className={css.HeaderPanel}>
            <div className="grid grid-cols-2 h-full w-full px-6 place-content-center">
                <div className="flex flex-row gap-10 h-6">
                    <img src={Logo} alt="logo" />
                    <img src={Menu} alt="menu" />
                </div>
                <div className="flex flex-row items-center justify-end gap-3 h-6 pr-10">
                    <div className={css.UsernameBox}>{takeIstLetters(user.firstName, user.lastName)}</div>
                    <span className={css.Username}>{ user?.firstName } { user?.lastName }</span>
                </div>
            </div>
        </header>
    );
};

export default MVPHeader
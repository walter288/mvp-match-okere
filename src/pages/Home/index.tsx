import React from 'react';
import ReportOneContainer from '../../components/core/container/ReportOne/Container';
import ProjectAndGatewayProvider from '../../provider/ProjectsAndGateway';
import ReportsProvider from '../../provider/Reports';

interface HomePageProps {};

const PHomePage = (props: HomePageProps) => {
    return (
        <ProjectAndGatewayProvider>
            <ReportsProvider>
                <ReportOneContainer />
            </ReportsProvider>
        </ProjectAndGatewayProvider>
    );
};

export default PHomePage;
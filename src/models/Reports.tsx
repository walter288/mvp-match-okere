export interface Reports {
    paymentId: string;
    amount: number;
    projectId: string;
    gatewayId: string;
    userIds: string[];
    modified: string;
    created: string;
  }

  export class ReportsPayload {
      from: string;
      to: string;
      projectId: string;
      gatewayId: string;

      constructor(from: string = '', to: string = '', projectId: string = '', gatewayId: string = '') {
          this.from = from;
          this.to = to;
          this.projectId = projectId;
          this.gatewayId = gatewayId;
      }
  }
export interface MVPUser {
    userId: string;
    firstName: string;
    lastName: string;
    email: string;
  }
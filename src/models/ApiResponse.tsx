export interface MVPApiResponse<T> {
    code: string;
    data: T[];
    error: unknown;
}

export const IsObject = (item: unknown): boolean => typeof item === 'object' && item !== null;

/**
 * Check if object is empty and we're covered for null and undefined. It will return false and not throw a TypeError.
 */
export const IsObjectEmpty = (value: object): boolean => {
  return (
    Object.prototype.toString.call(value) === '[object Object]' &&
    JSON.stringify(value) === '{}'
  );
}
